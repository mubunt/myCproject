//------------------------------------------------------------------------------
// Copyright (c) 2018-2019, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: myCproject
// Utility to create my usual tree structure of a small C project.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Note:
// We assume that we have enough memory space for our allocations; the result of
// the 'malloc' primitive is not tested.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <libgen.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <signal.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "myCproject_cmdline.h"
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#define	MACRO_PROJECTNAME			"%project%"
#define MACRO_DESCRIPTION			"%description%"
#define MACRO_AUTHOR				"%author%"
#define MACRO_YEAR					"%year%"
#define MACRO_LICENSE				"%license%"
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#define ProcessPseudoFileSystem		"/proc/self/exe"
#define BINDIR						"/myCproject_bin/"
#define CMD_YACOMMENT				"yaComment"
#define CMD_YATREE					"yaTree"
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#define error(fmt, ...) 			do { fprintf(stderr, "\nERROR: " fmt "\n\n", __VA_ARGS__); } while (0)
#define verbose(fmt, ...) 			do { fprintf(stdout, fmt "\n", __VA_ARGS__); } while (0)
#define title(s)					do { fprintf(stdout, "----------------------------------------- %s\n", s); fflush(stdout); } while (0)
#define EXIST(x)					(stat(x, ptlocstat)<0 ? 0:locstat.st_mode)
#define EXISTDIR(y)					(EXIST(y) && (locstat.st_mode & S_IFMT) == S_IFDIR)
#define PARSER 						cmdline_parser_myCproject
#define FREE 						cmdline_parser_myCproject_free
//------------------------------------------------------------------------------
// TYPEDEFS
//------------------------------------------------------------------------------
typedef enum { TOCOPY = 0, TOEDIT, TOEDIT_IF_EXE, TOEDIT_IF_LIB, TOIGNORE } file_action;
//------------------------------------------------------------------------------
// STRUCTURES
//------------------------------------------------------------------------------
struct s_datafile {
	char dataname[128];		// Filename located in BINDIR directory
	file_action action;		// What to do with this file
	char directory[128];	// directory name in project tree structure
	char name[128];			// Filename in project tree structure
};
//------------------------------------------------------------------------------
// LOCAL VARIABLES
//------------------------------------------------------------------------------
static struct gengetopt_args_info	args_info;
static char							datapath[PATH_MAX];
static char							projectpath[PATH_MAX];
static char							current_year[5];
static char 						*license = NULL;
static struct stat 					locstat;			// stat variables for EXIST*
static struct stat 					*ptlocstat;			// stat variables for EXIST*
//------------------------------------------------------------------------------
#include "myCproject.h"
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
bool can_run_command(const char *cmd) {
	if(strchr(cmd, '/')) {
		// if cmd includes a slash, no path search must be performed,
		// go straight to checking if it's executable
		return access(cmd, X_OK) == 0;
	}
	const char *path = getenv("PATH");
	if (!path) return false; // something is horribly wrong...
	// we are sure we won't need a buffer any longer
	char *buf = malloc(strlen(path)+strlen(cmd)+3);
	for (; *path; ++path) {
		// start from the beginning of the buffer
		char *p = buf;
		// copy in buf the current path element
		for (; *path && *path!=':'; ++path,++p) *p = *path;
		// empty path entries are treated like "."
		if (p==buf) *p++='.';
		// slash and command name
		if (p[-1]!='/') *p++='/';
		strcpy(p, cmd);
		// check if we can execute it
		if (access(buf, X_OK)==0) {
			free(buf);
			return true;
		}
		// quit at last cycle
		if (!*path) break;
	}
	// not found
	free(buf);
	return false;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void catchYear( char *year, size_t len ) {
	time_t timer;
	struct tm *tm_info;
	time(&timer);
	tm_info = localtime(&timer);
	strftime(year, len, "%Y", tm_info);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void catchLicense(char *path ) {
	char licfile[PATH_MAX * 2];
	long unsigned fsize;
	size_t unused;
	snprintf(licfile, sizeof(licfile), "%s/%s", path, dataFiles[0].dataname);
	FILE *f = fopen(licfile, "rb");
	if (f == NULL) {
		fsize = 1;
		license = malloc((fsize + 1) * sizeof(char));
		*license = '\0';
		return;
	}
	fseek(f, 0, SEEK_END);
	fsize = (long unsigned int) ftell(f);
	fseek(f, 0, SEEK_SET);  //same as rewind(f);
	license = malloc((fsize + 1) * sizeof(char));
	unused = fread(license, fsize, 1, f);
	fclose(f);
	license[fsize] = '\0';
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static bool mkdir_recursive(const char *path) {
	bool b = true;
	char *subpath, *fullpath;
	fullpath = strdup(path);
	subpath = dirname(fullpath);
	if (strlen(subpath) > 1) {
		b = mkdir_recursive(subpath);
		if (! b) return b;
	}
	if (! EXISTDIR(path)) {
		verbose("Creating directory %s", path);
		b = (0 == mkdir(path, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH));
	}
	free(fullpath);
	return b;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static size_t countChar( char *s, char c ) {
	size_t i, count;
	for (i = 0, count = 0; s[i]; i++) count += (s[i] == c);
	return count;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static char *replace_macro( const char *macro, char *content, char *expansion) {
#define HEADERSIZE 	64		// Max size of header line for multi-line expansion
	char *ptstart = content;
	char *ptcurrent = ptstart;
	char *pt;

	size_t nbl = 1;
	if (NULL != strchr(expansion, '\n'))
		nbl += countChar(expansion, '\n');

	while (NULL != (pt = strstr(ptcurrent, macro))) {
		char *newcontent = malloc((strlen(ptstart) + strlen(expansion) - strlen(macro) + nbl * HEADERSIZE) * sizeof(char));
		size_t len = (size_t)(pt - ptstart);
		strncpy(newcontent, ptstart, len);
		newcontent[len] = '\0';

		if (NULL != strchr(expansion, '\n')) {
			while (newcontent[len] != '\n' && len != 0) --len;
			char *ptmp = newcontent + len;
			char *header = malloc((strlen(ptmp) + 1) * sizeof(char));
			strcpy(header, ptmp);

			ptmp = expansion;
			char *ptmp2 = strchr(ptmp, '\n');
			*ptmp2 = '\0';
			strcat(newcontent, ptmp);
			*ptmp2 = '\n';
			ptmp = ptmp2 + 1;

			while (NULL != (ptmp2 = strchr(ptmp, '\n'))) {
				*ptmp2 = '\0';
				strcat(newcontent, header);
				strcat(newcontent, ptmp);
				*ptmp2 = '\n';
				ptmp = ptmp2 + 1;
			}

			if (strlen(ptmp) != 0) {
				strcat(newcontent, header);
				strcat(newcontent, ptmp);
			}

			free(header);
		} else
			strcat(newcontent, expansion);

		ptcurrent = newcontent + strlen(newcontent);
		strcat(newcontent, pt + strlen(macro));
		free(ptstart);
		ptstart = newcontent;
	}
	return ptstart;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static bool copyFile( char *from, char *to ) {
	size_t unused;
	FILE *f = fopen(from, "rb");
	if (f == NULL) return false;
	fseek(f, 0, SEEK_END);
	long unsigned fsize = (long unsigned int) ftell(f);
	fseek(f, 0, SEEK_SET);
	char *content;
	content = malloc((fsize + 1) * sizeof(char));
	unused = fread(content, fsize, 1, f);
	fclose(f);
	content[fsize] = '\0';
	f = fopen(to, "w");
	if (f == NULL) {
		free(content);
		return false;
	}
	fprintf(f, "%s", content);
	fclose(f);
	free(content);
	return true;
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static bool editFile( char *from, char *to ) {
	FILE *f;
	size_t unused;
	if (NULL == (f = fopen(from, "rb"))) return false;
	fseek(f, 0, SEEK_END);
	long unsigned fsize = (long unsigned int) ftell(f);
	fseek(f, 0, SEEK_SET);
	char *content;
	content = malloc((fsize + 1) * sizeof(char));
	unused = fread(content, fsize, 1, f);
	fclose(f);
	content[fsize] = '\0';

	char *pt = replace_macro(MACRO_PROJECTNAME, content, args_info.name_arg);
	pt = replace_macro(MACRO_DESCRIPTION, pt, args_info.description_arg);
	pt = replace_macro(MACRO_AUTHOR, pt,  args_info.author_arg);
	pt = replace_macro(MACRO_YEAR, pt, current_year);
	pt = replace_macro(MACRO_LICENSE, pt, license);

	if (NULL == (f = fopen(to, "w"))) {
		free(pt);
		return false;
	}
	fprintf(f, "%s", pt);
	fclose(f);
	free(pt);
	return true;
}
//------------------------------------------------------------------------------
// MAIN FUNCTION
//------------------------------------------------------------------------------
int main( int argc, char **argv ) {
	int ret, unused;
	//---- Initialization ------------------------------------------------------
	ptlocstat = &locstat;
	char cwd[PATH_MAX];
	if (getcwd(cwd, sizeof(cwd)) == NULL) {
		error("%s", "Cannot get current path.");
		return EXIT_FAILURE;
	}
	//---- Get real path of this executable to compute the path of data files
	if (readlink(ProcessPseudoFileSystem, datapath, sizeof(datapath)) == -1) {
		error("%s", "Cannot get path of the current executable.");
		return EXIT_FAILURE;
	}
	dirname(datapath);
	strcat(datapath, BINDIR);
	if (! EXISTDIR(datapath)) {
		error("WRONG INSTALLATION - Directory '%s' does not exist.", datapath);
		return EXIT_FAILURE;
	}
	//---- Check consistency of the installation
	int i = 0;
	char datafile[PATH_MAX * 2];
	while (strlen(dataFiles[i].dataname) != 0) {
		snprintf(datafile, sizeof(datafile), "%s%s", datapath, dataFiles[i].dataname);
		if (! EXIST(datafile)) {
			error("WRONG INSTALLATION - Data file '%s' does not exist.", datafile);
			return EXIT_FAILURE;
		}
		++i;
	}
	//---- Signals
	signal(SIGABRT, SIG_IGN);
	signal(SIGTERM, SIG_IGN);
	signal(SIGINT, SIG_IGN);
	//---- Parameter checking and setting
	if (PARSER(argc, argv, &args_info) != 0)
		return EXIT_FAILURE;
	// Project path name: args_info.path_arg
	// Project name: args_info.name_arg
	// Project one-line description: args_info.description_arg
	// Author's name: args_info.author_arg
	snprintf(projectpath, sizeof(projectpath), "%s/%s", args_info.path_arg, args_info.name_arg);
	if (EXISTDIR(projectpath)) {
		error("Project already existing %s.", projectpath);
		goto ERR;
	}
	if (args_info.exec_given && args_info.lib_given ) {
		error("%s.", "Executable or library, you have to make a choice");
		goto ERR;
	}
	catchYear(current_year, sizeof(current_year));
	catchLicense(datapath);
	//----  Tree sructure creation and initialization
	title("EXECUTION");
	if (! mkdir_recursive(projectpath)) {
		error("Cannot create directory %s.", projectpath);
		goto ERR;
	}
	i = 0;
	char destination[PATH_MAX * 4];
	while (strlen(dataFiles[i].dataname) != 0) {
		snprintf(datafile, sizeof(datafile), "%s%s", datapath, dataFiles[i].dataname);
		switch (dataFiles[i].action) {
		case TOCOPY:
			snprintf(destination, sizeof(destination), "%s/%s", projectpath, dataFiles[i].directory);
			if (! mkdir_recursive(destination)) {
				error("Cannot create directory %s.", destination);
				goto ERR;
			}
			snprintf(destination, sizeof(destination), "%s/%s/%s", projectpath, dataFiles[i].directory, dataFiles[i].dataname);
			verbose("Copying file %s", dataFiles[i].dataname);
			if (! copyFile(datafile, destination))
				error("Cannot copy file %s to %s", datafile, destination);
			break;
		case TOEDIT:
		case TOEDIT_IF_EXE:
		case TOEDIT_IF_LIB:
			if ((dataFiles[i].action == TOEDIT_IF_EXE && ! args_info.lib_given) ||
			        (dataFiles[i].action == TOEDIT_IF_LIB && args_info.lib_given) ||
			        dataFiles[i].action == TOEDIT) {
				snprintf(destination, sizeof(destination), "%s/%s", projectpath, dataFiles[i].directory);
				if (! mkdir_recursive(destination)) {
					error("Cannot create directory %s.", destination);
					goto ERR;
				}
				char tmpstr[PATH_MAX * 2];
				if (strncmp(dataFiles[i].name, MACRO_PROJECTNAME, strlen(MACRO_PROJECTNAME)) == 0) {
					snprintf(tmpstr, sizeof(tmpstr), "%s%s", args_info.name_arg, dataFiles[i].name + strlen(MACRO_PROJECTNAME));
					snprintf(destination, sizeof(destination), "%s/%s/%s", projectpath, dataFiles[i].directory, tmpstr);
				} else {
					//strncpy(tmpstr, dataFiles[i].name, sizeof(tmpstr));
					snprintf(tmpstr, sizeof(tmpstr), "%s", dataFiles[i].name);
					snprintf(destination, sizeof(destination), "%s/%s/%s", projectpath, dataFiles[i].directory, dataFiles[i].name);
				}
				verbose("Generating file %s/%s", dataFiles[i].directory, tmpstr);
				if (! editFile(datafile, destination))
					error("Cannot edit file %s to %s", datafile, destination);
			}
			break;
		default:
			break;
		}
		++i;
	}
	//---- Run 'yaComment' utility if it is accessible....
	if (can_run_command(CMD_YACOMMENT)) {
		title(CMD_YACOMMENT);
		char cmd1[PATH_MAX * 2];
		snprintf(cmd1, sizeof(cmd1), "%s %s", CMD_YACOMMENT, projectpath);
		unused = system(cmd1);
	}
	//---- Run 'yaTree' utility if it is accessible....
	if (can_run_command(CMD_YATREE)) {
		title(CMD_YATREE);
		char cmd2[PATH_MAX * 2];
		snprintf(cmd2, sizeof(cmd2), "%s %s", CMD_YATREE, projectpath);
		unused = system(cmd2);
	}
	title("END");
	//---- Exit ----------------------------------------------------------------
	ret = EXIT_SUCCESS;
EXIT:
	if (license != NULL) free(license);
	FREE(&args_info);
	signal(SIGABRT, SIG_DFL);
	signal(SIGTERM, SIG_DFL);
	signal(SIGINT, SIG_DFL);
	return ret;
ERR:
	ret = EXIT_FAILURE;
	goto EXIT;
}
//------------------------------------------------------------------------------
