//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: myCproject
// Utility to create my usual tree structure of a small C project.
//------------------------------------------------------------------------------
#ifndef MYCPROJECT_H
#define MYCPROJECT_H

static const struct s_datafile dataFiles[] = {
	{ "license.txt",		TOIGNORE,		"",			"" },				// Must be at index 0
	{ "COPYING.md",			TOCOPY,			".",		"" },
	{ "LICENSE.md",			TOCOPY,			".",		"" },
	{ "VERSION",			TOCOPY,			".",		"" },
	{ "data.mk",			TOCOPY,			".make",	"" },
	{ "execggo.mk",			TOCOPY,			".make",	"" },
	{ "exec.mk",			TOCOPY,			".make",	"" },
	{ "execw.mk",			TOCOPY,			".make",	"" },
	{ "help.mk",			TOCOPY,			".make",	"" },
	{ "helpw.mk",			TOCOPY,			".make",	"" },
	{ "java.mk",			TOCOPY,			".make",	"" },
	{ "javaw.mk",			TOCOPY,			".make",	"" },
	{ "jdata.mk",			TOCOPY,			".make",	"" },
	{ "jlib.mk",			TOCOPY,			".make",	"" },
	{ "jtopLevel.mk",		TOCOPY,			".make",	"" },
	{ "lib.mk",				TOCOPY,			".make",	"" },
	{ "srcLevel.mk",		TOCOPY,			".make",	"" },
	{ "topLevel.mk",		TOCOPY,			".make",	"" },
	{ "topLevelw.mk",		TOCOPY,			".make",	"" },
	{ "mainExe.c",			TOEDIT_IF_EXE,	"src",		"%project%.c" },
	{ "mainLib.c",			TOEDIT_IF_LIB,	"src",		"%project%.c" },
	{ "main.ggo",			TOEDIT_IF_EXE,	"src",		"%project%.ggo" },
	{ "README.md",			TOEDIT,			".",		"README.md" },
	{ "RELEASENOTES.md",	TOEDIT,			".",		"RELEASENOTES.md" },
	{ "topMakefile",		TOEDIT,			".",		"Makefile" },
	{ "srcMakefileExe",		TOEDIT_IF_EXE,	"src",		"Makefile" },
	{ "srcMakefileLib",		TOEDIT_IF_LIB,	"src",		"Makefile" },
	{ "gitignore",			TOEDIT,			".",		".gitignore" },
	{ "",					TOIGNORE,		"",			"" }
};

#endif	// MYCPROJECT_H