 # *%project%*, %description%.

***...TO BE COMPLETED...***

## LICENSE
**%project%** is covered by the GNU General Public License (GPL) version 3 and above.

## USAGE
``` bash
$ %project% -h

$ %project% -V

$ %project% ..............

```
## STRUCTURE OF THE APPLICATION
This section walks you through **%project%**'s structure. Once you understand this structure, you will easily find your way around in **%project%**'s code base.

``` bash
$ yaTree
./                        # Application level
........
$ 
```
## HOW TO BUILD THIS APPLICATION
```Shell
$ cd %project%
$ make clean all
```

## HOW TO INSTALL AND USE THIS APPLICATION
```Shell
$ cd %project%
$ make release
    # Library generated with -O2 option and the header file, are respectively installed in $LIB_DIR and $INC_DIR directories (defined at environment level).

    # Executable generated with -O2 option, is installed in $BIN_DIR directory (defined at environment level). We consider that $BIN_DIR is a part of the PATH.
```

## NOTES

## SOFTWARE REQUIREMENTS
- For usage, nothing particular...
- For development:
   - *GengetOpt* binary package installed, version 2.22.6.
- Developped and tested on XUBUNTU 19.04, GCC v8.3.0

## RELEASE NOTES
Refer to file [RELEASENOTES](./RELEASENOTES.md).

***