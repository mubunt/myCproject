# *myCproject*, a utility to create my usual tree structure of a small C project.

It's a simple exercise that makes my life easier to initialize the file tree of a new C project of the type I usually develop. This utility is easily adaptable by customizing either the *src/myCproject.h* file or the models and templates of the *data* directory.

## LICENSE
**myCproject** is covered by the GNU General Public License (GPL) version 3 and above.

## USAGE
``` bash
$ myCproject -h
myCproject - Copyright (c) 2018, Michel RIZZO. All Rights Reserved.
myCproject - Version 1.0.11

Utility to create my usual tree structure of a small C project.

Usage: myCproject [OPTIONS]...

  -h, --help                Print help and exit
  -V, --version             Print version and exit
  -n, --name=DIRECTORY      Project name.
  -p, --path=PATH           Project path name.
  -d, --description=STRING  Project one-line description.
  -a, --author=STRING       Author's name.
      --exec                Output of the project is an executable
                              (default=on)
      --lib                 Output of the project is a library  (default=off)

Exit: returns a non-zero status if an error is detected.

$ myCproject -V
myCproject - Copyright (c) 2018, Michel RIZZO. All Rights Reserved.
myCproject - Version 1.0.0
$ myCproject -n MyProject -d "This is a trial of small C project" -a "Michel RIZZO" -p ~/tmp
----------------------------------------- EXECUTION
Creating directory /home/michel/tmp/MyProject
Copying file COPYING.md
Copying file LICENSE.md
Copying file VERSION
Creating directory /home/michel/tmp/MyProject/src
Generating file src/MyProject.c
Generating file src/MyProject.ggo
Generating file ./README.md
Generating file ./RELEASENOTES.md
Generating file ./Makefile
Generating file src/Makefile
----------------------------------------- yaComment
Directory .
Directory ./src
----------------------------------------- yaTree
/home/michel/tmp/MyProject/ # Application level
├── src/                    # Source directory
│   ├── Makefile            # Makefile
│   ├── MyProject.c         # 
│   └── MyProject.ggo       # 'gengetopt' option definition. Refer to https://www.gnu.org/software/gengetopt/gengetopt.html
├── COPYING.md              # GNU General Public License markdown file
├── LICENSE.md              # License markdown file
├── Makefile                # Makefile
├── README.md               # ReadMe markdown file
├── RELEASENOTES.md         # Release Notes markdown file
└── VERSION                 # Version identification text file

1 directories, 9 files
----------------------------------------- END
$
```
## STRUCTURE OF THE APPLICATION
This section walks you through **myCproject**'s structure. Once you understand this structure, you will easily find your way around in **myCproject**'s code base.

``` bash
$ yaTree
./                      # Application level
├── data/               # Templates and models
│   ├── COPYING.md      # Model of  GNU General Public License markdown file
│   ├── LICENSE.md      # Model of  License markdown file
│   ├── Makefile        # Makefile
│   ├── README.md       # Template of ReadMe markdown file
│   ├── RELEASENOTES.md # Template of Release Notes markdown file
│   ├── VERSION         # Model for Version identification text file
│   ├── data.mk         # Portion of makefile for data
│   ├── exec.mk         # Portion of makefile for C executable
│   ├── execggo.mk      # Portion of makefile for C executable with ggo
│   ├── gitignore       # Model of git file
│   ├── java.mk         # Portion of makefile for java compilation
│   ├── jdata.mk        # Portion of makefile for java data
│   ├── jtopLevel.mk    # Portion of makefile for top-level Makefile fir java project
│   ├── lib.mk          # Portion of makefile forC library
│   ├── license.txt     # Model of license text to insert in each source file 
│   ├── main.ggo        # Template for 'gengetopt' option definition
│   ├── mainExe.c       # Template of main programme (for executable, located in src)
│   ├── mainLib.c       # Template of a function (for library, located in src)
│   ├── srcLevel.mk     # Portion of makefile for src-level Makefile (project with several outputs)
│   ├── srcMakefileExe  # Template of src/Makefile (for executable)
│   ├── srcMakefileLib  # Template of src/Makefile (for library)
│   ├── topLevel.mk     # Portion of makefile for top-level Makefile
│   └── topMakefile     # Template for top-level Makefile
├── src/                # Source directory
│   ├── Makefile        # Makefile
│   ├── myCproject.c    # Main program
│   ├── myCproject.ggo  # 'gengetopt' option definition. Refer to https://www.gnu.org/software/gengetopt/gengetopt.html
│   └── myCproject.h    # Header file to customize for personal project
├── COPYING.md          # GNU General Public License markdown file
├── LICENSE.md          # License markdown file
├── Makefile            # Makefile
├── README.md           # ReadMe markdown file
├── RELEASENOTES.md     # Release Notes markdown file
└── VERSION             # Version identification text file

2 directories, 33 files
$
```
## HOW TO BUILD THIS APPLICATION
```Shell
$ cd myCproject
$ make clean all
```

## HOW TO INSTALL AND USE THIS APPLICATION
```Shell
$ cd myCproject
$ make release
    # Executable generated with -O2 option, is installed in $BIN_DIR directory (defined at environment level).
    # We consider that $BIN_DIR is a part of the PATH.
```
## NOTES

## SOFTWARE REQUIREMENTS
- For usage, nothing particular...
- For development:
   - *GengetOpt* binary package installed, version 2.22.6.
- yaComment utility: https://gitlab.com/mubunt/yaComment
- yaTree utility: https://gitlab.com/mubunt/yaTree
- Developped and tested on XUBUNTU 18.04, GCC v7.3.0

## RELEASE NOTES
Refer to file [RELEASENOTES](./RELEASENOTES.md).

***