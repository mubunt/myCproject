# RELEASE NOTES: *myCproject*, a utility to create my usual tree structure of a small C project.

Functional limitations, if any, of this version are described in the *README.md* file.

- **Version 1.0.20**:
  - Updated build system components.

- **Version 1.0.19**:
  - Fixed list of *mk* files to be installed in *data/Makefile*.

- **Version 1.0.18**:
  - Updated build system.
  - Added new *.mk* files used by **jemma** project.

- **Version 1.0.17**:
  - Documented *.mk* files: context of usage and inputs.

- **Version 1.0.16**:
  - Removed unused files.

- **Version 1.0.15**:
  - Updated build system component(s).

- **Version 1.0.14**:
  - Fixed *data/data.mk* file.

- **Version 1.0.13**:
  - Reworked build system to ease global and inter-project updated.
  - Run *astyle* on sources.
  - Added *cppcheck* target (Static C code analysis) and run it.
  - Adapted *myCproject* to the new build system.

- **Version 1.0.12**:
  - Updated templates for *Makefile* (lib and exec).
  - Updated template for *library main source* file.

- **Version 1.0.11**:
  - Updated template for *README.md* file.
  - Fixed help message.

- **Version 1.0.10**:
  - Standardization of the installation of executables and libraries in $BIN_DIR, $LIB_DIR anetd $INC_DIR defined in the environment.
  - Added option for output of the project: executable or library

- **Version 1.0.9**:
  - Fixed generated top level makefile (release target).

- **Version 1.0.8**:
  - Updated for using the last syntax of *yaComment* v1.2.x.

- **Version 1.0.7**:
  - Fixed issue when the project path is "./something".

- **Version 1.0.6**:
  - Fixed ./Makefile and ./src/Makefile.
  - Fixed accordingly some data files used to generate Makefiles.
  - Removed compilation warnings in ./src/myCproject.c.

**Version 1.0.5:**
  - Added *.gitignore* file at top level.
  - Added *gitignore* template; will be renamed as *.gitignore* file in new project.

**Version 1.0.4:**
  - Replaced *${OPTIM}* with *$(OPTIM)* in makefile template (*src* level).

**Version 1.0.3:**
  - Updated top level makefile template for the release tagging.

**Version 1.0.2:**
  - Added tagging of new release.

**Version 1.0.1:**
  - Updated template (*main.c*).

**Version 1.0.0:**
  - First version.
